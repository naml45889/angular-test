import { Component, OnInit } from '@angular/core';
import {User} from '../user';
import {UserService} from '../user.service';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-update-user',
  templateUrl: './update-user.component.html',
  styleUrls: ['./update-user.component.css']
})
export class UpdateUserComponent implements OnInit {

  id: number;
  // @ts-ignore
  user: User = new User();
  constructor(private userService: UserService, private route: ActivatedRoute, private router: Router) {
    this.id = this.route.snapshot.params.id;
    this.userService.getUserById(this.id).subscribe(data => {
      this.user = data;
    },
      error => console.log(error));
  }
  // tslint:disable-next-line:typedef
  onSubmit(){
    this.userService.updateUser(this.id, this.user).subscribe(data => {
      this.goToUserList();
    },
      error => console.log(error));
  }
  ngOnInit(): void {
    this.id = this.route.snapshot.params.id;
    this.userService.getUserById(this.id).subscribe(data => {
      this.user = data;
    },
      error => console.log(error));
  }

  // tslint:disable-next-line:typedef
  goToUserList(){
    this.router.navigate(['/user']);
  }
}
