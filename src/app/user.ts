export class User {
  id: number;
  name: string;
  address: string;
  age: number;
  position: string;
  sex: number;
  salary: number;
  company: string;
  status: string;

  // tslint:disable-next-line:max-line-length
  constructor(id: number, name: string, address: string, age: number, position: string, sex: number, salary: number, company: string, status: string) {
    this.id = id;
    this.name = name;
    this.address = address;
    this.age = age;
    this.position = position;
    this.sex = sex;
    this.salary = salary;
    this.company = company;
    this.status = status;
  }

}
