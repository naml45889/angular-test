import {Component, OnInit} from '@angular/core';
import {User} from '../user';
import {UserService} from '../user.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-create-user',
  templateUrl: './create-user.component.html',
  styleUrls: ['./create-user.component.css']
})
export class CreateUserComponent implements OnInit {

  // @ts-ignore
  user: User = new User();

  constructor(private userService: UserService, private router: Router) {
  }

  ngOnInit(): void {
  }

  // tslint:disable-next-line:typedef
  saveUser() {
    this.userService.createUser(this.user).subscribe(data => {
        this.goToUserList();
      },
      error => {
        console.log(error);
      });
  }

  // tslint:disable-next-line:typedef
  goToUserList() {
    this.router.navigate(['/user']);
  }

  // tslint:disable-next-line:typedef
  onSubmit() {
    console.log(this.user);
    this.saveUser();
  }
}
