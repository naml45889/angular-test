import {Component, OnInit} from '@angular/core';
import {User} from '../user';
import {UserService} from '../user.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
export class UserListComponent implements OnInit {

  users: User[] = [];

  constructor(private userService: UserService, private router: Router) {
  }

  ngOnInit(): void {
    this.getUsers();
  }

  // tslint:disable-next-line:typedef
  private getUsers() {
    this.userService.getUsersList().subscribe(data => {
      this.users = data;
    });
  }
  // tslint:disable-next-line:typedef
  updateUser(id: number){
    // @ts-ignore
    this.router.navigate(['update', id]);
  }
  // tslint:disable-next-line:typedef
  getUserById(id: number){
    // @ts-ignore
    this.router.navigate(['user', id]);
  }
  // tslint:disable-next-line:typedef
  deleteUser(id: number){
    this.userService.deleteUser(id).subscribe(data => {
      this.getUsers();
    });
  }
}
